# filters

## uBlacklist.txt

__[uBlacklist.txt raw](https://gitlab.com/takuoh/filters/-/raw/master/uBlacklist.txt)__

## Ublock Origin

__[UblockOrigin.txt raw](https://gitlab.com/takuoh/filters/-/raw/master/UblockOrigin.txt)__

## pawoo hosts

__[pawoo.txt raw](https://gitlab.com/takuoh/filters/-/raw/master/pawoo.txt)__

## Recommends

https://raw.githubusercontent.com/robonxt/CleanYourTwitter/master/CleanYourTwitter.txt

https://raw.githack.com/bogachenko/fuckfuckadblock/master/fuckfuckadblock.txt

https://raw.githubusercontent.com/tofukko/filter/master/Adblock_Plus_list.txt
